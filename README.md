#### Overdamped Brownian particle in symmetric piecewise linear potential:  
$`\dot{x} = -V'(x) + \xi(t)`$
